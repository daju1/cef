﻿// Copyright © 2010-2014 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.

using System;
using System.Windows.Forms;
using CefSharp.Example;
using HtmlAgilityPack;

namespace CefSharp.WinForms.Parser
{
    public partial class BrowserForm : Form
    {
        private const string DefaultUrlForAddedTabs = "https://www.google.com";
        private const string KateClappUrlForAddedTabs = "http://instagram.com/kateclapp";
        private CefSharp.MyParserTask.FilterParam m_param = new CefSharp.MyParserTask.FilterParam();
        private AccountAddData m_account_data = new AccountAddData();


        public BrowserForm()
        {
            InitializeComponent();

            var bitness = Environment.Is64BitProcess ? "x64" : "x86";
            Text = "CefSharp.WinForms.Parser - " + bitness;
            WindowState = FormWindowState.Maximized;

            //AddTab(CefExample.DefaultUrl);

            //Only perform layout when control has completly finished resizing
            ResizeBegin += (s, e) => SuspendLayout();
            ResizeEnd += (s, e) => ResumeLayout(true);

            textBoxDirOut.Text = "D:/___out";
            dateTimePickerMax.Value = DateTime.Now;
            dateTimePickerMin.Value = DateTime.Now - TimeSpan.FromDays(365);
            /*
            textBoxAccountsList.Text += "http://instagram.com/justinbieber" + "\n";
            textBoxAccountsList.Text += "http://instagram.com/kateclapp" + "\n";
            textBoxAccountsList.Text += "http://instagram.com/starbucks" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/fitnessgirlsmotivation" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/fitness" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/instagramfitness" + "\n";
            */
            textBoxAccountsList.Text += "https://instagram.com/fitnessgirlsmotivation" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/fitness" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/instagramfitness" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/instafemmefitness" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/fitmotivation" + "\n";
            textBoxAccountsList.Text += "https://instagram.com/xfitofficial" + "\n";

            textBoxStopWords.Text += "привет" + "\n";
            textBoxStopWords.Text += "солнце" + "\n";

            textBoxAccountStopWords.Text += "http://vk.com/*" + "\n";;
            textBoxEmailes.Text += "dsfasfasdf@yandex.ru" + "\n";
            textBoxPrefixes.Text += "hi." + "\n";

            comboBoxEncoding.Items.Add("Default");
            comboBoxEncoding.Items.Add("ASCII");
            comboBoxEncoding.Items.Add("Unicode");
            comboBoxEncoding.Items.Add("BigEndianUnicode");
            comboBoxEncoding.Items.Add("UTF32");
            comboBoxEncoding.Items.Add("UTF7");
            comboBoxEncoding.Items.Add("UTF8");
            comboBoxEncoding.SelectedItem = "UTF8";
        }

        private void AddTab(string url, int? insertIndex = null)
        {
            browserTabControl.SuspendLayout();

            var browser = new BrowserTabUserControl(url)
            {
                Dock = DockStyle.Fill,
            };

            var tabPage = new TabPage(url)
            {
                Dock = DockStyle.Fill
            };

            //This call isn't required for the sample to work. 
            //It's sole purpose is to demonstrate that #553 has been resolved.
            browser.CreateControl();

            tabPage.Controls.Add(browser);

            if (insertIndex == null)
            {
                browserTabControl.TabPages.Add(tabPage);
            }
            else
            {
                browserTabControl.TabPages.Insert(insertIndex.Value, tabPage);
            }

            //Make newly created tab active
            browserTabControl.SelectedTab = tabPage;

            browserTabControl.ResumeLayout(true);
        }

        private void ExitMenuItemClick(object sender, EventArgs e)
        {
            ExitApplication();
        }

        private void ExitApplication()
        {
            Cef.Shutdown();
            Close();
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void FindMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.ShowFind();
            }
        }

        private void CopySourceToClipBoardAsyncClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.CopySourceToClipBoardAsync();
            }
        }

        private BrowserTabUserControl GetCurrentTabControl()
        {
            if (browserTabControl.SelectedIndex == -1)
            {
                return null;
            }

            var tabPage = browserTabControl.Controls[browserTabControl.SelectedIndex];
            var control = (BrowserTabUserControl)tabPage.Controls[0];

            return control;
        }

        private void NewTabToolStripMenuItemClick(object sender, EventArgs e)
        {
            AddTab(DefaultUrlForAddedTabs);
        }

        private void CloseTabToolStripMenuItemClick(object sender, EventArgs e)
        {
            if(browserTabControl.Controls.Count == 0)
            {
                return;
            }

            var currentIndex = browserTabControl.SelectedIndex;

            var tabPage = browserTabControl.Controls[currentIndex];

            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Dispose();
            }

            browserTabControl.Controls.Remove(tabPage);

            browserTabControl.SelectedIndex = currentIndex - 1;

            if (browserTabControl.Controls.Count == 0)
            {
                ExitApplication();
            }
        }

        private void UndoMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Undo();
            }
        }

        private void RedoMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Redo();
            }
        }

        private void CutMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Cut();
            }
        }

        private void CopyMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Copy();
            }
        }

        private void PasteMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Paste();
            }
        }

        private void DeleteMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Delete();
            }
        }

        private void SelectAllMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.SelectAll();
            }
        }

        private void PrintToolStripMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Print();
            }
        }

        private void ShowDevToolsMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.ShowDevTools();
            }
        }

        private void CloseDevToolsMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.CloseDevTools();
            }
        }

        private void kateClappToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddTab(KateClappUrlForAddedTabs);
        }

        private void emmaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddTab("http://instagram.com/sel_queen_192");
        }

        private void buttonGetHtml_Click(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control == null)
                return;

            var task = control.Browser.GetSourceAsync();
            task.Wait();

            string html = task.Result;
            //textBoxHTML.Text = html;
            //labelHTMLSize.Text = html.Length.ToString();
            
            string html2 = "";
            for (int i = 0; i < html.Length; ++i )
            {
                html2 += html[i];
                if (html[i] == '>')
                    html2 += "\n";
            }
            //textBoxHTML2.Text = html2;            
        }

        public BrowserTabUserControl Parsing(string account)
        {
            int cnt = browserTabControl.Controls.Count;
            AddTab(account);

            var tabPage = browserTabControl.Controls[cnt];
            var control = (BrowserTabUserControl)tabPage.Controls[0];

            return control;
        }

        public void DisposeParsing(int currentIndex )
        {
            if (browserTabControl.Controls.Count == 0)
            {
                return;
            }

            var tabPage = browserTabControl.Controls[currentIndex];

            var control = (BrowserTabUserControl)tabPage.Controls[0];
            if (control != null)
            {
                control.Dispose();
            }

            browserTabControl.Controls.Remove(tabPage);

            //browserTabControl.SelectedIndex = currentIndex - 1;
        }

        private static void ParseList(string words, ref System.Collections.Generic.List<string> list)
        {
            string word;

            int _i2 = words.IndexOf('\n');
            while (_i2 > 0 || words.Length > 0)
            {
                if (_i2 > 0)
                    word = words.Substring(0, _i2);
                else
                {
                    word = words;
                    words = "";
                }

                words = words.Substring(_i2 + 1);
                _i2 = words.IndexOf('\n');


                int i3 = word.IndexOf('\r');
                if (i3 > 0)
                    word = word.Substring(0, i3);

                int i4 = word.IndexOf('\n');
                if (i4 > 0)
                    word = word.Substring(0, i4);

                word.Trim();

                if (word.Length > 0)
                    list.Add(word);
            }
        }

        private void buttonParse_Click(object sender, EventArgs e)
        {
            InitDebugWindows();

            m_param.need_fotos = System.Int32.Parse(textBoxNeedFotosNumber.Text);
            m_param.use_likes_comments_nuber_filter = checkBoxUseLikesCommentsFilter.Checked;

            m_param.use_time_interval_filter = checkBoxUseTimeIntervalFilter.Checked;

            if (m_param.use_time_interval_filter)
            {
                m_param.time_max = dateTimePickerMax.Value;
                m_param.time_min = dateTimePickerMin.Value;
            }

            m_param.use_stop_words_filter = checkBoxUseStopWordsFilter.Checked;
            if (m_param.use_stop_words_filter)
            {
                ParseList(textBoxStopWords.Text, ref m_param.stop_words);
            }

            ParseList(textBoxAccountsList.Text, ref m_account_data.accounts);           
            ParseList(textBoxAccountStopWords.Text, ref m_account_data.stop_words);
            ParseList(textBoxPrefixes.Text, ref m_account_data.prefixes);
            ParseList(textBoxEmailes.Text, ref m_account_data.emailes);
            
            bool bSavePictures = checkBoxSavePictures.Checked;
            bool bSaveProfiles = checkBoxSaveProfiles.Checked;



            string folder = textBoxDirOut.Text;
            int i = 0;
            //try
            //{
            //    System.Threading.Thread ThA = new System.Threading.Thread(
            //        () =>
            //        {
                        foreach (string account in m_account_data.accounts)
                        {
                            try
                            {
                                int startIndex = account.LastIndexOf('/');
                                if (startIndex == -1)
                                    continue;
                                string name = account.Substring(startIndex);
                                string path = folder + name;
                                System.IO.Directory.CreateDirectory(path);

                                BrowserTabUserControl control1 = Parsing("");
                                BrowserTabUserControl control2 = Parsing("");

                                MyBrowserTask browser_task = new MyBrowserTask(this, account, m_param, folder, path, m_account_data, i, bSavePictures, bSaveProfiles, ref control1, ref control2);

                                DisposeParsing(1);
                                DisposeParsing(0);

                                ++i;
                            }
                            catch (Exception ex)
                            {
                                ParsingErrorsLog(account, ex.Message, true);
                            }

                        }

            //        }
            //    );

            //    ThA.Start();
            //}
            //catch(Exception ex)
            //{
            //    textBoxParsingErrorsLog.Text += ex.Message + "\n";
            //}
        }

        private void buttonScript_Click(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control == null)
                return;

            var script = CefSharp.MyParserTask.FirstParser.script;
            var task = control.Browser.EvaluateScriptAsync(script);
            task.Wait();
        }

        private void buttonBrowseOutputDirectory_Click(object sender, EventArgs e)
        {
            DialogResult = folderBrowserDialog1.ShowDialog();
            if (DialogResult == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;
                textBoxDirOut.Text = path;
            }
        }

        private int nEndLoadingNumber = 0;
        private int nStartParsingNumber = 0;
        private int nEndParsingNumber = 0;
        private int nParsingErrorsLogNumber = 0;

        private void InitDebugWindows()
        {
            textBoxEndLoading.Text = "";
            textBoxStartParsing.Text = "";
            textBoxEndParsing.Text = "";
            textBoxParsingErrorsLog.Text = "";

            nEndLoadingNumber = 0;
            nStartParsingNumber = 0;
            nEndParsingNumber = 0;
            nParsingErrorsLogNumber = 0;

            labelEndLoadingNumber.Text = "0";
            labelStartParsingNumber.Text = "0";
            labelEndParsingNumber.Text = "0";
            labelParsingErrorsNumber.Text = "0";
        }

        public void EndLoading(string url)
        {
            textBoxEndLoading.BeginInvoke(new MethodInvoker(() => textBoxEndLoading.Text += url + "\n"));
            nEndLoadingNumber++;
            labelEndLoadingNumber.BeginInvoke(new MethodInvoker(() => labelEndLoadingNumber.Text = nEndLoadingNumber.ToString()));
        }
        public void StartParsing(string url)
        {
            textBoxStartParsing.BeginInvoke(new MethodInvoker(() => textBoxStartParsing.Text += url + "\n"));
            nStartParsingNumber++;
            labelStartParsingNumber.BeginInvoke(new MethodInvoker(() => labelStartParsingNumber.Text = nStartParsingNumber.ToString()));
        }

        public void EndParsing(string url)
        {
            textBoxEndParsing.BeginInvoke(new MethodInvoker(() => textBoxEndParsing.Text += url + "\n"));
            nEndParsingNumber++;
            labelEndParsingNumber.BeginInvoke(new MethodInvoker(() => labelEndParsingNumber.Text = nEndParsingNumber.ToString()));
        }

        public void ParsingErrorsLog(string url, string message, bool toIncrement)
        {
            if (!toIncrement)
                message = "while avatar saving" + message;
            textBoxParsingErrorsLog.BeginInvoke(new MethodInvoker(() => textBoxParsingErrorsLog.Text += url + " " + message + "\n"));
            if (toIncrement)
                nParsingErrorsLogNumber++;
            labelParsingErrorsNumber.BeginInvoke(new MethodInvoker(() => labelParsingErrorsNumber.Text = nParsingErrorsLogNumber.ToString()));
        }

        private void comboBoxEncoding_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(comboBoxEncoding.SelectedItem.ToString())
            {
                case "Default":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.Default;
                    }
                    break;
                case "ASCII":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.ASCII;
                    }
                    break;
                case "Unicode":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.Unicode;
                    }
                    break;
                case "BigEndianUnicode":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.BigEndianUnicode;
                    }
                    break;
                case "UTF32":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.UTF32;
                    }
                    break;
                case "UTF7":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.UTF7;
                    }
                    break;
                case "UTF8":
                    {
                        CefSharp.MyParserTask.FirstParser.m_encoding = System.Text.Encoding.UTF8;
                    }
                    break;
            }
        }
    }

}
