﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CefSharp.Example;
using HtmlAgilityPack;

namespace CefSharp.WinForms.Parser
{
    public class AccountAddData
    {
        public List<string> accounts = new List<string>();
        public List<string> stop_words = new List<string>();
        public List<string> prefixes = new List<string>();
        public List<string> emailes = new List<string>();
        public AccountAddData()
        {

        }

        public string GetPrefix(int i)
        {
            if (prefixes.Count == accounts.Count)
            {
                if (i < prefixes.Count)
                    return prefixes[i];
            }
            else if (prefixes.Count < accounts.Count && prefixes.Count > 0)
            {
                Random rnd = new Random();
                int j = rnd.Next(0, prefixes.Count - 1);
                return prefixes[j];
            }
            return "";
        }
        public string GetEmail(int i)
        {
            if (emailes.Count == accounts.Count)
            {
                if (i < emailes.Count)
                    return emailes[i];
            }
            else if (emailes.Count < accounts.Count && emailes.Count > 0)
            {
                Random rnd = new Random();
                int j = rnd.Next(0, emailes.Count - 1);
                return emailes[j];
            }
            return "";
        }
    }

    class MyBrowserTask
    {
        //private static ChromiumWebBrowser browser;

        private BrowserForm m_form;
        private string m_folder;
        private string m_path;
        private CefSharp.MyParserTask.FilterParam m_param;

        private AccountAddData m_account_data;
        private int m_i;

        private string m_log;

        bool m_bSavePictures;
        bool m_bSaveProfiles;

        BrowserTabUserControl m_control1;
        BrowserTabUserControl m_control2;

        private System.Threading.EventWaitHandle m_event = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset);

        public MyBrowserTask(BrowserForm form, string url, CefSharp.MyParserTask.FilterParam param, string folder, string path, AccountAddData account_data, int i, bool bSavePictures, bool bSaveProfiles, ref BrowserTabUserControl control1, ref BrowserTabUserControl control2)
        {
            m_i = i;
            m_account_data = account_data;
            m_folder = folder;
            m_param = param;
            m_form = form;
            m_path = path;

            m_bSavePictures = bSavePictures;
            m_bSaveProfiles = bSaveProfiles;

            m_control1 = control1;
            m_control2 = control2;

            // You need to replace this with your own call to Cef.Initialize();
            //CefExample.Init();

            //m_form.BeginInvoke(new System.Windows.Forms.MethodInvoker(() =>
            //{
                //m_control = m_form.Parsing(url);
                //m_control2 = m_form.Parsing("");
                ChromiumWebBrowser browser = (ChromiumWebBrowser)m_control1.Browser;
                // An event that is fired when the first page is finished loading.
                // This returns to us from another thread.
                browser.FrameLoadEnd += BrowserFrameLoadEnd;

                browser.Load(url);
            //}));


            m_event.WaitOne();
        }    
 
 
        public bool ProcessAccountFile(CefSharp.MyParserTask.AccountParam param)
        {
            string filtered_description = param.FilterDescription(m_account_data.stop_words);
            
            string path = m_folder + "/accounts.csv";
            System.IO.FileStream stream;
            int i = 0;
            while (true)
            {
                try
                {
                    stream = System.IO.File.Open(path, System.IO.FileMode.Append);
                    break;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    System.Threading.Thread.Sleep(100);
                    if (++i > 100)
                        return false;
                }
            }

            /* Encoding.UTF8*/
            System.IO.StreamWriter accounts = new System.IO.StreamWriter(stream, CefSharp.MyParserTask.FirstParser.m_encoding);
            string line = "\"" + param.fio + "\";;" + m_account_data.GetPrefix(m_i) + param.username + ";;" + m_account_data.GetEmail(m_i) + ";\"" + param.avatar_save_path + "\";;\"" + filtered_description + "\"";
            accounts.WriteLine(line);
            accounts.Close();
            stream.Close();

            return true;
        }

        private void BrowserFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            // Check to ensure it is the main frame which has finished loading
            // (rather than an iframe within the main frame).
            if (e.IsMainFrame)
            {
                ChromiumWebBrowser browser = (ChromiumWebBrowser)sender;

                // Remove the load event handler, because we only want one snapshot of the initial page.
                browser.FrameLoadEnd -= BrowserFrameLoadEnd;

                 // проверим наличие контекста синхронизации
                var context0 = System.Threading.SynchronizationContext.Current;
                if (context0 == null)
                {
                    // http://habrahabr.ru/post/232169/
                    // создадим новый контекст синхронизации для текущего потока
                    var context = new System.Threading.SynchronizationContext();
                    // зададим контекст синхронизации текущему потоку
                    System.Threading.SynchronizationContext.SetSynchronizationContext(context);
                }

                m_form.EndLoading(browser.Address);

                var task = browser.GetSourceAsync();

                task.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        string html = task.Result;

                        m_form.StartParsing(browser.Address);

                        try
                        {

                            if (m_bSaveProfiles)
                            {    
                                CefSharp.MyParserTask.AccountParam param = new CefSharp.MyParserTask.AccountParam();
                                CefSharp.MyParserTask.FirstParser.GetAccountParam(html, ref param);
                                

                                try
                                {
                                    int index = param.avatar_url.LastIndexOf('.');
                                    if (index > -1)
                                    {
                                        string extension = param.avatar_url.Substring(index);
                                        param.avatar_save_path = m_folder + "/" + param.username + "_avatar" + extension;
                                        System.Net.WebClient client = new System.Net.WebClient();

                                        Console.WriteLine();
                                        Console.WriteLine("Saving avatar image {0} ", param.avatar_save_path);

                                        m_log += " Saving avatar ";

                                        client.DownloadFile(param.avatar_url, param.avatar_save_path);

                                        m_log += "Saved avatar ";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    m_form.ParsingErrorsLog(browser.Address, ex.Message + m_log, false);
                                }

                                ProcessAccountFile(param);
                            }

                            if (m_bSavePictures)
                            {
                                ChromiumWebBrowser browser2 = (ChromiumWebBrowser)m_control2.Browser;

                                CefSharp.MyParserTask.FirstParser.Parsing(html, m_param, m_path, null, browser, ref m_log, ref browser2);
                            }

                            m_form.EndParsing(browser.Address + m_log);


                        }
                        catch(Exception ex)
                        {
                            m_form.ParsingErrorsLog(browser.Address, ex.Message + m_log, true);
                        }



                        m_event.Set();

                    }
                },
                System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }
        }


    }
}
