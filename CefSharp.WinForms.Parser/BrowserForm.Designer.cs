﻿namespace CefSharp.WinForms.Parser
{
    partial class BrowserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BrowserForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showDevToolsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeDevToolsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.cutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.copySourceToClipBoardAsyncMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instagrammToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kateClappToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browserTabControl = new System.Windows.Forms.TabControl();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxSaveProfiles = new System.Windows.Forms.CheckBox();
            this.checkBoxSavePictures = new System.Windows.Forms.CheckBox();
            this.labelParsingErrorsNumber = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxParsingErrorsLog = new System.Windows.Forms.TextBox();
            this.labelEndParsingNumber = new System.Windows.Forms.Label();
            this.labelStartParsingNumber = new System.Windows.Forms.Label();
            this.labelEndLoadingNumber = new System.Windows.Forms.Label();
            this.textBoxEndLoading = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxStartParsing = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxEndParsing = new System.Windows.Forms.TextBox();
            this.textBoxAccountStopWords = new System.Windows.Forms.TextBox();
            this.textBoxPrefixes = new System.Windows.Forms.TextBox();
            this.textBoxEmailes = new System.Windows.Forms.TextBox();
            this.buttonBrowseOutputDirectory = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDirOut = new System.Windows.Forms.TextBox();
            this.textBoxStopWords = new System.Windows.Forms.TextBox();
            this.checkBoxUseStopWordsFilter = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerMin = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerMax = new System.Windows.Forms.DateTimePicker();
            this.checkBoxUseTimeIntervalFilter = new System.Windows.Forms.CheckBox();
            this.checkBoxUseLikesCommentsFilter = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNeedFotosNumber = new System.Windows.Forms.TextBox();
            this.buttonParse = new System.Windows.Forms.Button();
            this.textBoxAccountsList = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.comboBoxEncoding = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.instagrammToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(861, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTabToolStripMenuItem,
            this.closeTabToolStripMenuItem,
            this.printToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.showDevToolsMenuItem,
            this.closeDevToolsMenuItem,
            this.toolStripMenuItem3,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newTabToolStripMenuItem
            // 
            this.newTabToolStripMenuItem.Name = "newTabToolStripMenuItem";
            this.newTabToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.newTabToolStripMenuItem.Text = "&New Tab";
            this.newTabToolStripMenuItem.Click += new System.EventHandler(this.NewTabToolStripMenuItemClick);
            // 
            // closeTabToolStripMenuItem
            // 
            this.closeTabToolStripMenuItem.Name = "closeTabToolStripMenuItem";
            this.closeTabToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.closeTabToolStripMenuItem.Text = "&Close Tab";
            this.closeTabToolStripMenuItem.Click += new System.EventHandler(this.CloseTabToolStripMenuItemClick);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.printToolStripMenuItem.Text = "&Print";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.PrintToolStripMenuItemClick);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // showDevToolsMenuItem
            // 
            this.showDevToolsMenuItem.Name = "showDevToolsMenuItem";
            this.showDevToolsMenuItem.Size = new System.Drawing.Size(158, 22);
            this.showDevToolsMenuItem.Text = "Show Dev Tools";
            this.showDevToolsMenuItem.Click += new System.EventHandler(this.ShowDevToolsMenuItemClick);
            // 
            // closeDevToolsMenuItem
            // 
            this.closeDevToolsMenuItem.Name = "closeDevToolsMenuItem";
            this.closeDevToolsMenuItem.Size = new System.Drawing.Size(158, 22);
            this.closeDevToolsMenuItem.Text = "Close Dev Tools";
            this.closeDevToolsMenuItem.Click += new System.EventHandler(this.CloseDevToolsMenuItemClick);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(155, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitMenuItemClick);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoMenuItem,
            this.redoMenuItem,
            this.findMenuItem,
            this.toolStripMenuItem2,
            this.cutMenuItem,
            this.copyMenuItem,
            this.pasteMenuItem,
            this.deleteMenuItem,
            this.selectAllMenuItem,
            this.toolStripSeparator1,
            this.copySourceToClipBoardAsyncMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoMenuItem
            // 
            this.undoMenuItem.Name = "undoMenuItem";
            this.undoMenuItem.Size = new System.Drawing.Size(251, 22);
            this.undoMenuItem.Text = "Undo";
            this.undoMenuItem.Click += new System.EventHandler(this.UndoMenuItemClick);
            // 
            // redoMenuItem
            // 
            this.redoMenuItem.Name = "redoMenuItem";
            this.redoMenuItem.Size = new System.Drawing.Size(251, 22);
            this.redoMenuItem.Text = "Redo";
            this.redoMenuItem.Click += new System.EventHandler(this.RedoMenuItemClick);
            // 
            // findMenuItem
            // 
            this.findMenuItem.Name = "findMenuItem";
            this.findMenuItem.Size = new System.Drawing.Size(251, 22);
            this.findMenuItem.Text = "Find";
            this.findMenuItem.Click += new System.EventHandler(this.FindMenuItemClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(248, 6);
            // 
            // cutMenuItem
            // 
            this.cutMenuItem.Name = "cutMenuItem";
            this.cutMenuItem.Size = new System.Drawing.Size(251, 22);
            this.cutMenuItem.Text = "Cut";
            this.cutMenuItem.Click += new System.EventHandler(this.CutMenuItemClick);
            // 
            // copyMenuItem
            // 
            this.copyMenuItem.Name = "copyMenuItem";
            this.copyMenuItem.Size = new System.Drawing.Size(251, 22);
            this.copyMenuItem.Text = "Copy";
            this.copyMenuItem.Click += new System.EventHandler(this.CopyMenuItemClick);
            // 
            // pasteMenuItem
            // 
            this.pasteMenuItem.Name = "pasteMenuItem";
            this.pasteMenuItem.Size = new System.Drawing.Size(251, 22);
            this.pasteMenuItem.Text = "Paste";
            this.pasteMenuItem.Click += new System.EventHandler(this.PasteMenuItemClick);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(251, 22);
            this.deleteMenuItem.Text = "Delete";
            this.deleteMenuItem.Click += new System.EventHandler(this.DeleteMenuItemClick);
            // 
            // selectAllMenuItem
            // 
            this.selectAllMenuItem.Name = "selectAllMenuItem";
            this.selectAllMenuItem.Size = new System.Drawing.Size(251, 22);
            this.selectAllMenuItem.Text = "Select All";
            this.selectAllMenuItem.Click += new System.EventHandler(this.SelectAllMenuItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(248, 6);
            // 
            // copySourceToClipBoardAsyncMenuItem
            // 
            this.copySourceToClipBoardAsyncMenuItem.Name = "copySourceToClipBoardAsyncMenuItem";
            this.copySourceToClipBoardAsyncMenuItem.Size = new System.Drawing.Size(251, 22);
            this.copySourceToClipBoardAsyncMenuItem.Text = "Copy Source to Clipboard (async)";
            this.copySourceToClipBoardAsyncMenuItem.Click += new System.EventHandler(this.CopySourceToClipBoardAsyncClick);
            // 
            // instagrammToolStripMenuItem
            // 
            this.instagrammToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kateClappToolStripMenuItem,
            this.emmaToolStripMenuItem});
            this.instagrammToolStripMenuItem.Name = "instagrammToolStripMenuItem";
            this.instagrammToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.instagrammToolStripMenuItem.Text = "Instagram";
            // 
            // kateClappToolStripMenuItem
            // 
            this.kateClappToolStripMenuItem.Name = "kateClappToolStripMenuItem";
            this.kateClappToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.kateClappToolStripMenuItem.Text = "Kate Clapp";
            this.kateClappToolStripMenuItem.Click += new System.EventHandler(this.kateClappToolStripMenuItem_Click);
            // 
            // emmaToolStripMenuItem
            // 
            this.emmaToolStripMenuItem.Name = "emmaToolStripMenuItem";
            this.emmaToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.emmaToolStripMenuItem.Text = "Emma";
            this.emmaToolStripMenuItem.Click += new System.EventHandler(this.emmaToolStripMenuItem_Click);
            // 
            // browserTabControl
            // 
            this.browserTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.browserTabControl.Location = new System.Drawing.Point(288, 39);
            this.browserTabControl.Name = "browserTabControl";
            this.browserTabControl.SelectedIndex = 0;
            this.browserTabControl.Size = new System.Drawing.Size(573, 984);
            this.browserTabControl.TabIndex = 2;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 24);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(282, 999);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.comboBoxEncoding);
            this.panel1.Controls.Add(this.checkBoxSaveProfiles);
            this.panel1.Controls.Add(this.checkBoxSavePictures);
            this.panel1.Controls.Add(this.labelParsingErrorsNumber);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.textBoxParsingErrorsLog);
            this.panel1.Controls.Add(this.labelEndParsingNumber);
            this.panel1.Controls.Add(this.labelStartParsingNumber);
            this.panel1.Controls.Add(this.labelEndLoadingNumber);
            this.panel1.Controls.Add(this.textBoxEndLoading);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBoxStartParsing);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxEndParsing);
            this.panel1.Controls.Add(this.textBoxAccountStopWords);
            this.panel1.Controls.Add(this.textBoxPrefixes);
            this.panel1.Controls.Add(this.textBoxEmailes);
            this.panel1.Controls.Add(this.buttonBrowseOutputDirectory);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxDirOut);
            this.panel1.Controls.Add(this.textBoxStopWords);
            this.panel1.Controls.Add(this.checkBoxUseStopWordsFilter);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.checkBoxUseTimeIntervalFilter);
            this.panel1.Controls.Add(this.checkBoxUseLikesCommentsFilter);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxNeedFotosNumber);
            this.panel1.Controls.Add(this.buttonParse);
            this.panel1.Controls.Add(this.textBoxAccountsList);
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(282, 984);
            this.panel1.TabIndex = 5;
            // 
            // checkBoxSaveProfiles
            // 
            this.checkBoxSaveProfiles.AutoSize = true;
            this.checkBoxSaveProfiles.Checked = true;
            this.checkBoxSaveProfiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSaveProfiles.Location = new System.Drawing.Point(164, 458);
            this.checkBoxSaveProfiles.Name = "checkBoxSaveProfiles";
            this.checkBoxSaveProfiles.Size = new System.Drawing.Size(88, 17);
            this.checkBoxSaveProfiles.TabIndex = 33;
            this.checkBoxSaveProfiles.Text = "Save Profiles";
            this.checkBoxSaveProfiles.UseVisualStyleBackColor = true;
            // 
            // checkBoxSavePictures
            // 
            this.checkBoxSavePictures.AutoSize = true;
            this.checkBoxSavePictures.Checked = true;
            this.checkBoxSavePictures.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSavePictures.Location = new System.Drawing.Point(16, 459);
            this.checkBoxSavePictures.Name = "checkBoxSavePictures";
            this.checkBoxSavePictures.Size = new System.Drawing.Size(92, 17);
            this.checkBoxSavePictures.TabIndex = 32;
            this.checkBoxSavePictures.Text = "Save Pictures";
            this.checkBoxSavePictures.UseVisualStyleBackColor = true;
            // 
            // labelParsingErrorsNumber
            // 
            this.labelParsingErrorsNumber.AutoSize = true;
            this.labelParsingErrorsNumber.Location = new System.Drawing.Point(153, 908);
            this.labelParsingErrorsNumber.Name = "labelParsingErrorsNumber";
            this.labelParsingErrorsNumber.Size = new System.Drawing.Size(13, 13);
            this.labelParsingErrorsNumber.TabIndex = 31;
            this.labelParsingErrorsNumber.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 908);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Parsing Errors Log";
            // 
            // textBoxParsingErrorsLog
            // 
            this.textBoxParsingErrorsLog.Location = new System.Drawing.Point(12, 924);
            this.textBoxParsingErrorsLog.Multiline = true;
            this.textBoxParsingErrorsLog.Name = "textBoxParsingErrorsLog";
            this.textBoxParsingErrorsLog.ReadOnly = true;
            this.textBoxParsingErrorsLog.Size = new System.Drawing.Size(261, 57);
            this.textBoxParsingErrorsLog.TabIndex = 29;
            // 
            // labelEndParsingNumber
            // 
            this.labelEndParsingNumber.AutoSize = true;
            this.labelEndParsingNumber.Location = new System.Drawing.Point(154, 823);
            this.labelEndParsingNumber.Name = "labelEndParsingNumber";
            this.labelEndParsingNumber.Size = new System.Drawing.Size(13, 13);
            this.labelEndParsingNumber.TabIndex = 28;
            this.labelEndParsingNumber.Text = "0";
            // 
            // labelStartParsingNumber
            // 
            this.labelStartParsingNumber.AutoSize = true;
            this.labelStartParsingNumber.Location = new System.Drawing.Point(154, 752);
            this.labelStartParsingNumber.Name = "labelStartParsingNumber";
            this.labelStartParsingNumber.Size = new System.Drawing.Size(13, 13);
            this.labelStartParsingNumber.TabIndex = 27;
            this.labelStartParsingNumber.Text = "0";
            // 
            // labelEndLoadingNumber
            // 
            this.labelEndLoadingNumber.AutoSize = true;
            this.labelEndLoadingNumber.Location = new System.Drawing.Point(154, 675);
            this.labelEndLoadingNumber.Name = "labelEndLoadingNumber";
            this.labelEndLoadingNumber.Size = new System.Drawing.Size(13, 13);
            this.labelEndLoadingNumber.TabIndex = 26;
            this.labelEndLoadingNumber.Text = "0";
            // 
            // textBoxEndLoading
            // 
            this.textBoxEndLoading.Location = new System.Drawing.Point(13, 694);
            this.textBoxEndLoading.Multiline = true;
            this.textBoxEndLoading.Name = "textBoxEndLoading";
            this.textBoxEndLoading.ReadOnly = true;
            this.textBoxEndLoading.Size = new System.Drawing.Size(261, 55);
            this.textBoxEndLoading.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 675);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "End Loading";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 823);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "End Parsing";
            // 
            // textBoxStartParsing
            // 
            this.textBoxStartParsing.Location = new System.Drawing.Point(12, 768);
            this.textBoxStartParsing.Multiline = true;
            this.textBoxStartParsing.Name = "textBoxStartParsing";
            this.textBoxStartParsing.ReadOnly = true;
            this.textBoxStartParsing.Size = new System.Drawing.Size(261, 52);
            this.textBoxStartParsing.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 752);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Start Parsing";
            // 
            // textBoxEndParsing
            // 
            this.textBoxEndParsing.Location = new System.Drawing.Point(13, 839);
            this.textBoxEndParsing.Multiline = true;
            this.textBoxEndParsing.Name = "textBoxEndParsing";
            this.textBoxEndParsing.ReadOnly = true;
            this.textBoxEndParsing.Size = new System.Drawing.Size(261, 57);
            this.textBoxEndParsing.TabIndex = 20;
            // 
            // textBoxAccountStopWords
            // 
            this.textBoxAccountStopWords.Location = new System.Drawing.Point(13, 603);
            this.textBoxAccountStopWords.Multiline = true;
            this.textBoxAccountStopWords.Name = "textBoxAccountStopWords";
            this.textBoxAccountStopWords.Size = new System.Drawing.Size(261, 56);
            this.textBoxAccountStopWords.TabIndex = 19;
            // 
            // textBoxPrefixes
            // 
            this.textBoxPrefixes.Location = new System.Drawing.Point(12, 541);
            this.textBoxPrefixes.Multiline = true;
            this.textBoxPrefixes.Name = "textBoxPrefixes";
            this.textBoxPrefixes.Size = new System.Drawing.Size(261, 56);
            this.textBoxPrefixes.TabIndex = 18;
            // 
            // textBoxEmailes
            // 
            this.textBoxEmailes.Location = new System.Drawing.Point(12, 479);
            this.textBoxEmailes.Multiline = true;
            this.textBoxEmailes.Name = "textBoxEmailes";
            this.textBoxEmailes.Size = new System.Drawing.Size(261, 56);
            this.textBoxEmailes.TabIndex = 17;
            // 
            // buttonBrowseOutputDirectory
            // 
            this.buttonBrowseOutputDirectory.Location = new System.Drawing.Point(246, 430);
            this.buttonBrowseOutputDirectory.Name = "buttonBrowseOutputDirectory";
            this.buttonBrowseOutputDirectory.Size = new System.Drawing.Size(27, 23);
            this.buttonBrowseOutputDirectory.TabIndex = 16;
            this.buttonBrowseOutputDirectory.Text = "...";
            this.buttonBrowseOutputDirectory.UseVisualStyleBackColor = true;
            this.buttonBrowseOutputDirectory.Click += new System.EventHandler(this.buttonBrowseOutputDirectory_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 414);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Output directory";
            // 
            // textBoxDirOut
            // 
            this.textBoxDirOut.Location = new System.Drawing.Point(16, 430);
            this.textBoxDirOut.Name = "textBoxDirOut";
            this.textBoxDirOut.Size = new System.Drawing.Size(211, 20);
            this.textBoxDirOut.TabIndex = 14;
            // 
            // textBoxStopWords
            // 
            this.textBoxStopWords.Location = new System.Drawing.Point(16, 357);
            this.textBoxStopWords.Multiline = true;
            this.textBoxStopWords.Name = "textBoxStopWords";
            this.textBoxStopWords.Size = new System.Drawing.Size(258, 45);
            this.textBoxStopWords.TabIndex = 13;
            // 
            // checkBoxUseStopWordsFilter
            // 
            this.checkBoxUseStopWordsFilter.AutoSize = true;
            this.checkBoxUseStopWordsFilter.Location = new System.Drawing.Point(15, 334);
            this.checkBoxUseStopWordsFilter.Name = "checkBoxUseStopWordsFilter";
            this.checkBoxUseStopWordsFilter.Size = new System.Drawing.Size(129, 17);
            this.checkBoxUseStopWordsFilter.TabIndex = 12;
            this.checkBoxUseStopWordsFilter.Text = "Use Stop Words Filter";
            this.checkBoxUseStopWordsFilter.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimePickerMin);
            this.groupBox1.Controls.Add(this.dateTimePickerMax);
            this.groupBox1.Location = new System.Drawing.Point(15, 227);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 101);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Time Interval";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Min";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Max";
            // 
            // dateTimePickerMin
            // 
            this.dateTimePickerMin.Location = new System.Drawing.Point(58, 61);
            this.dateTimePickerMin.Name = "dateTimePickerMin";
            this.dateTimePickerMin.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerMin.TabIndex = 1;
            // 
            // dateTimePickerMax
            // 
            this.dateTimePickerMax.Location = new System.Drawing.Point(58, 19);
            this.dateTimePickerMax.Name = "dateTimePickerMax";
            this.dateTimePickerMax.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerMax.TabIndex = 0;
            // 
            // checkBoxUseTimeIntervalFilter
            // 
            this.checkBoxUseTimeIntervalFilter.AutoSize = true;
            this.checkBoxUseTimeIntervalFilter.Location = new System.Drawing.Point(15, 204);
            this.checkBoxUseTimeIntervalFilter.Name = "checkBoxUseTimeIntervalFilter";
            this.checkBoxUseTimeIntervalFilter.Size = new System.Drawing.Size(133, 17);
            this.checkBoxUseTimeIntervalFilter.TabIndex = 10;
            this.checkBoxUseTimeIntervalFilter.Text = "Use Time interval Filter";
            this.checkBoxUseTimeIntervalFilter.UseVisualStyleBackColor = true;
            // 
            // checkBoxUseLikesCommentsFilter
            // 
            this.checkBoxUseLikesCommentsFilter.AutoSize = true;
            this.checkBoxUseLikesCommentsFilter.Location = new System.Drawing.Point(15, 181);
            this.checkBoxUseLikesCommentsFilter.Name = "checkBoxUseLikesCommentsFilter";
            this.checkBoxUseLikesCommentsFilter.Size = new System.Drawing.Size(211, 17);
            this.checkBoxUseLikesCommentsFilter.TabIndex = 9;
            this.checkBoxUseLikesCommentsFilter.Text = "Use Likes and Comments Number Filter";
            this.checkBoxUseLikesCommentsFilter.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Need Fotos Number";
            // 
            // textBoxNeedFotosNumber
            // 
            this.textBoxNeedFotosNumber.Location = new System.Drawing.Point(182, 155);
            this.textBoxNeedFotosNumber.Name = "textBoxNeedFotosNumber";
            this.textBoxNeedFotosNumber.Size = new System.Drawing.Size(100, 20);
            this.textBoxNeedFotosNumber.TabIndex = 7;
            this.textBoxNeedFotosNumber.Text = "100";
            // 
            // buttonParse
            // 
            this.buttonParse.Location = new System.Drawing.Point(199, 665);
            this.buttonParse.Name = "buttonParse";
            this.buttonParse.Size = new System.Drawing.Size(75, 23);
            this.buttonParse.TabIndex = 5;
            this.buttonParse.Text = "Parse";
            this.buttonParse.UseVisualStyleBackColor = true;
            this.buttonParse.Click += new System.EventHandler(this.buttonParse_Click);
            // 
            // textBoxAccountsList
            // 
            this.textBoxAccountsList.Location = new System.Drawing.Point(3, 18);
            this.textBoxAccountsList.Multiline = true;
            this.textBoxAccountsList.Name = "textBoxAccountsList";
            this.textBoxAccountsList.Size = new System.Drawing.Size(276, 131);
            this.textBoxAccountsList.TabIndex = 4;
            // 
            // comboBoxEncoding
            // 
            this.comboBoxEncoding.FormattingEnabled = true;
            this.comboBoxEncoding.Location = new System.Drawing.Point(152, 403);
            this.comboBoxEncoding.Name = "comboBoxEncoding";
            this.comboBoxEncoding.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEncoding.TabIndex = 34;
            this.comboBoxEncoding.SelectedIndexChanged += new System.EventHandler(this.comboBoxEncoding_SelectedIndexChanged);
            // 
            // BrowserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 1023);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.browserTabControl);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BrowserForm";
            this.Text = "BrowserForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem showDevToolsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem findMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem copySourceToClipBoardAsyncMenuItem;
        private System.Windows.Forms.TabControl browserTabControl;
        private System.Windows.Forms.ToolStripMenuItem newTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeDevToolsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instagrammToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kateClappToolStripMenuItem;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonParse;
        private System.Windows.Forms.TextBox textBoxAccountsList;
        private System.Windows.Forms.ToolStripMenuItem emmaToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNeedFotosNumber;
        private System.Windows.Forms.CheckBox checkBoxUseLikesCommentsFilter;
        private System.Windows.Forms.CheckBox checkBoxUseTimeIntervalFilter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerMin;
        private System.Windows.Forms.DateTimePicker dateTimePickerMax;
        private System.Windows.Forms.CheckBox checkBoxUseStopWordsFilter;
        private System.Windows.Forms.TextBox textBoxStopWords;
        private System.Windows.Forms.Button buttonBrowseOutputDirectory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDirOut;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox textBoxAccountStopWords;
        private System.Windows.Forms.TextBox textBoxPrefixes;
        private System.Windows.Forms.TextBox textBoxEmailes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxEndParsing;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxStartParsing;
        private System.Windows.Forms.TextBox textBoxEndLoading;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelEndParsingNumber;
        private System.Windows.Forms.Label labelStartParsingNumber;
        private System.Windows.Forms.Label labelEndLoadingNumber;
        private System.Windows.Forms.Label labelParsingErrorsNumber;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxParsingErrorsLog;
        private System.Windows.Forms.CheckBox checkBoxSaveProfiles;
        private System.Windows.Forms.CheckBox checkBoxSavePictures;
        private System.Windows.Forms.ComboBox comboBoxEncoding;

    }
}