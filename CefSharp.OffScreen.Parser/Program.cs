﻿// Copyright © 2010-2014 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.

using System;
using System.Diagnostics;
using System.IO;
using CefSharp.Example;

namespace CefSharp.OffScreen.Parser
{
    public class Program
    {
        //private static ChromiumWebBrowser browser;

        private static CefSharp.MyParserTask.FilterParam m_param = new CefSharp.MyParserTask.FilterParam();

        private static string m_folder;
        private static string m_path;
        private static string m_log;
        public static void Main(string[] args)
        {
            /*StreamReader FileReader = new StreamReader(@"D:/MyFile.txt");
            string FileContents;
            FileContents = FileReader.ReadToEnd();
            FileReader.Close();*/

            m_folder = @"D:/___out";

            const string testUrl = "http://instagram.com/kateclapp";
            //const string testUrl = "http://instagram.com/ellieseit";
            //const string testUrl = "http://instagram.com/sel_queen_192";

            // You need to replace this with your own call to Cef.Initialize();
            CefExample.Init();

            Browse(testUrl);

            // We have to wait for something, otherwise the process will exit too soon.
            Console.ReadKey();

            // Clean up Chromium objects.  You need to call this in your application otherwise
            // you will get a crash when closing.
            Cef.Shutdown();
        }

        private static void Browse(string testUrl)
        {

            string name = testUrl.Substring(testUrl.LastIndexOf('/'));
            m_path = m_folder + name;
            try
            {
                System.IO.Directory.CreateDirectory(m_path);
            }
            catch(Exception ex)
            {
                m_folder = @"D:/___out";
                m_path = m_folder + name;
                System.IO.Directory.CreateDirectory(m_path);
            }

            // Create the offscreen Chromium browser.
            ChromiumWebBrowser browser = new ChromiumWebBrowser();

            // An event that is fired when the first page is finished loading.
            // This returns to us from another thread.
            browser.FrameLoadEnd += BrowserFrameLoadEnd;

            // Start loading the test URL in Chrome's thread.
            browser.Load(testUrl);
        }

        private static void BrowserFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            // Check to ensure it is the main frame which has finished loading
            // (rather than an iframe within the main frame).
            if (e.IsMainFrame)
            {

                ChromiumWebBrowser browser = (ChromiumWebBrowser)sender;

                // Remove the load event handler, because we only want one snapshot of the initial page.
                browser.FrameLoadEnd -= BrowserFrameLoadEnd;

                // http://habrahabr.ru/post/232169/                
                // проверим наличие контекста синхронизации
                var context0 = System.Threading.SynchronizationContext.Current;
                if (context0 == null)
                {
                    // создадим новый контекст синхронизации для текущего потока
                    var context = new System.Threading.SynchronizationContext();
                    // зададим контекст синхронизации текущему потоку
                    System.Threading.SynchronizationContext.SetSynchronizationContext(context);
                }
 
                var task_s = browser.GetSourceAsync();

                task_s.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        string html = task_s.Result;
                        CefSharp.WinForms.ChromiumWebBrowser win_form_browser = null;
                        CefSharp.WinForms.ChromiumWebBrowser win_form_browser2 = null;
                        CefSharp.MyParserTask.FirstParser.Parsing(html, m_param, m_path, browser, win_form_browser, ref m_log, ref win_form_browser2);
                        browser.Dispose();
                    }
                },
                System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }
        }
    }
}
