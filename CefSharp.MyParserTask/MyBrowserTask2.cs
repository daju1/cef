﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CefSharp.Example;
using HtmlAgilityPack;

namespace CefSharp.MyParserTask
{
    public class MyBrowserTask2
    {
        //private CefSharp.OffScreen.ChromiumWebBrowser browser;
        private CefSharp.WinForms.ChromiumWebBrowser browser;

        public string m_html;

        private System.Threading.EventWaitHandle m_event2 = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.ManualReset);


        public MyBrowserTask2(string url, ref CefSharp.WinForms.ChromiumWebBrowser browser2)
        {

            // You need to replace this with your own call to Cef.Initialize();
            //CefExample.Init();

            Console.WriteLine();
            Console.WriteLine("MyBrowserTask2 {0}", url);


            //browser = new CefSharp.WinForms.ChromiumWebBrowser();
            //browser = new CefSharp.OffScreen.ChromiumWebBrowser();
            browser = browser2;

            // An event that is fired when the first page is finished loading.
            // This returns to us from another thread.
            browser.FrameLoadEnd += BrowserFrameLoadEnd2;

            // Start loading the test URL in Chrome's thread.
            browser.Load(url);

            Console.WriteLine("m_event2.WaitOne();");
            m_event2.WaitOne();
            Console.WriteLine("post m_event2.WaitOne();");
        }


        private void BrowserFrameLoadEnd2(object sender, FrameLoadEndEventArgs e)
        {
            // Check to ensure it is the main frame which has finished loading
            // (rather than an iframe within the main frame).
            if (e.IsMainFrame)
            {
                // Remove the load event handler, because we only want one snapshot of the initial page.
                browser.FrameLoadEnd -= BrowserFrameLoadEnd2;

                Console.WriteLine();
                Console.WriteLine("BrowserFrameLoadEnd2 if (e.IsMainFrame)");
                                
                // проверим наличие контекста синхронизации
                var context0 = System.Threading.SynchronizationContext.Current;
                if (context0 == null)
                {
                    // создадим новый контекст синхронизации для текущего потока
                    var context = new System.Threading.SynchronizationContext();
                    // зададим контекст синхронизации текущему потоку
                    System.Threading.SynchronizationContext.SetSynchronizationContext(context);
                }

                var task_i = browser.GetSourceAsync();

                task_i.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        m_html = task_i.Result;
                        //browser.Dispose();

                        Console.WriteLine("browser.Dispose(); ");
                        m_event2.Set();
                    }
                },
                System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        public static string GoodHtml(string html)
        {
            string html_2 = "";
            for (int i = 0; i < html.Length; ++i)
            {
                html_2 += html[i];
                if (html[i] == '>')
                    html_2 += "\n";
            }
            return html_2;
        }

        public static bool GetImageDescription(string html_i, ref string image_description)
        {
            Console.WriteLine();
            Console.WriteLine("Parsing html_i");

            HtmlAgilityPack.HtmlDocument htdoc_i = new HtmlAgilityPack.HtmlDocument();
            htdoc_i.LoadHtml(html_i);
            
            //<meta name="description" content="«Join The March 16th Challenge! . TAP the link in their profile @FitGirlsGuide for details! 💕💪 . www.FitGirlsGuide.com . THE 28 DAY JUMPSTART IS ... 💟…»" />
            //<meta property="og:site_name" content="Instagram" />
            //<meta property="og:title" content="Fitness &amp; Health в Instagram: «Join The March 16th Challenge! . TAP the link in their profile @FitGirlsGuide for details! 💕💪 . www.FitGirlsGuide.com . THE 28 DAY…»" />
            //<meta property="og:description" content="«Join The March 16th Challenge! . TAP the link in their profile @FitGirlsGuide for details! 💕💪 . www.FitGirlsGuide.com . THE 28 DAY JUMPSTART IS ... 💟…»" />

            //<h1 class="sCaption" data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1">
            //<span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$end:0">Hard work. Dedication</span></h1>

            //<h1 class="sCaption" data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1">
            //<span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$text0:0">
            //Самое главное — это терпение! ☝Многие сдаются, ломаются и проигрывают из-за него. 
            //Ты всегда должна помнить — всему свое время😉</span>
            //<span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$entity0:0">#40кгвтренде</span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$text1:0"> </span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$entity1:0">#подкачайсясахарочек</span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$text2:0"> </span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$entity2:0">#спорт</span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$text3:0"> </span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$entity3:0">#фитнес</span><span data-reactid=".0.0.1.0.0.0.0.0.0.1.0.0.0:0.1.2:1.$text0:0:$end:0">💚</span></h1>

            HtmlNode node = htdoc_i.DocumentNode.SelectSingleNode("//h1[@class='sCaption']");
            if (node == null)
            {
                Console.WriteLine();
                Console.WriteLine("node == null)");
                Console.WriteLine("GetImageDescription() return;");
                return false;
            }

            image_description = node.InnerText;

            return true;
        }

        public static bool GetImageUrl(string html_i, ref string image_url)
        {
            Console.WriteLine();
            Console.WriteLine("Parsing html_i");

            HtmlAgilityPack.HtmlDocument htdoc_i = new HtmlAgilityPack.HtmlDocument();
            htdoc_i.LoadHtml(html_i);

            //<div src="https://igcdn-photos-e-a.akamaihd.net/hphotos-ak-xaf1/t51.2885-15/10956267_849453641807980_1486655426_n.jpg" class="lfFrame Frame Image" data-reactid=".0.0.1.0.0.0.0.0.0.0:$frame935540760098800895:0.0"><div data-reactid=".0.0.1.0.0.0.0.0.0.0:$frame935540760098800895:0.0.0"><div class="iImage" id="iImage_0" style="background-image:url(https://igcdn-photos-e-a.akamaihd.net/hphotos-ak-xaf1/t51.2885-15/10956267_849453641807980_1486655426_n.jpg);" data-reactid=".0.0.1.0.0.0.0.0.0.0:$frame935540760098800895:0.0.0.$=1$iImage_0"></div></div></div>

            HtmlNode node_i = htdoc_i.DocumentNode.SelectSingleNode("//meta[@property='og:image']");
            if (node_i == null)
            {
                Console.WriteLine();
                Console.WriteLine("node_i == null)");
                Console.WriteLine("GetImageUrl() html_i return;");
                return false;
            }

            HtmlAttributeCollection attributeCollection_i = node_i.Attributes;
            if (attributeCollection_i == null)
            {
                Console.WriteLine();
                Console.WriteLine("attributeCollection_i == null");
                Console.WriteLine("GetImageUrl() html_i return;");
                return false;
            }

            bool res = false;
            foreach (HtmlAttribute attribute_i in attributeCollection_i.AttributesWithName("content"))
            {
                image_url = attribute_i.Value;
                res = true;
            }
            return res;
        }

        public static string SavingImage(string image_url, string _out_folder)
        {
            System.Net.WebClient client = new System.Net.WebClient();

            char[] anyOf = { '/', '\\' };
            string fn = image_url.Substring(image_url.LastIndexOfAny(anyOf));
                
            var downloadPath = _out_folder + fn;

            Console.WriteLine();
            Console.WriteLine("Saving image {0} ", downloadPath);
            Console.WriteLine();

            client.DownloadFile(image_url, downloadPath);

            return downloadPath;
        }
    }
}
