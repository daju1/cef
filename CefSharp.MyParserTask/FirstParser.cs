﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CefSharp.MyParserTask
{
    public class PhotoGridMediaItem
    {
        public bool bLoad = false;
        public DateTime time = DateTime.MinValue;
        public int nLikes = 0;
        public int nComments = 0;
        public string image_page_link = "";
        public string image_url = "";
        public string image_description = "";

        public PhotoGridMediaItem()
        {

        }
    }

    public class FilterParam
    {
        public int nLevelMax10Likes = 0;
        public int nLevelMax10Comments = 0;
        public DateTime time_min = DateTime.MinValue;
        public DateTime time_max = DateTime.MaxValue;

        public int need_fotos = 100;
        public bool use_likes_comments_nuber_filter = true;
        public bool use_time_interval_filter = true;
        public bool use_stop_words_filter = false;
        public List<string> stop_words = new List<string>();

        public FilterParam()
        {

        }
    }

    public class AccountParam
    {
        public string username = "";
        public string avatar_url = "";
        public string fio = "";
        public string description = "";
        public string avatar_save_path = "";

        public AccountParam()
        {

        }

        public string FilterDescription(List<string> stop_words)
        {
            string filtered_description = description;
            foreach (string stop_word in stop_words)
            {
                string pattern = stop_word.Replace("*", @"([^\s]+)");
                filtered_description = System.Text.RegularExpressions.Regex.Replace(filtered_description, pattern, "");
                //filtered_description = filtered_description.Replace('\n', ' ');
            }

            return filtered_description;
        }

    }


    public class FirstParser
    {
        /* <button class="PhotoGridMoreButton pgmbDisabled" data-reactid=".0.0.1.3.0.1.0" disabled="true"> <span class="pgmbLabel pgmbLabelDisabled" data-reactid=".0.0.1.3.0.1.0.1"> All items loaded</span></button>*/
        public static string script = string.Format("document.getElementsByClassName('PhotoGridMoreButton')[0].click();");
        public static int nMillisecondsSleep = 5000;
        public static System.Text.Encoding m_encoding = System.Text.Encoding.UTF8;

        public static void GetAccountParam (string html, ref AccountParam param)
        {
            param.username = "";
            param.avatar_url = "";
            param.fio = "";
            param.description = "";

            HtmlAgilityPack.HtmlDocument htdoc = new HtmlAgilityPack.HtmlDocument();
            htdoc.LoadHtml(html);

            HtmlAgilityPack.HtmlNode node_username = htdoc.DocumentNode.SelectSingleNode("//span[@class='Username']");
            if (node_username != null)
            {
                HtmlAgilityPack.HtmlDocument htdoc_i = new HtmlAgilityPack.HtmlDocument();
                htdoc_i.LoadHtml(node_username.InnerHtml);

                HtmlAgilityPack.HtmlNode node_username_i = htdoc_i.DocumentNode.SelectSingleNode("//a[@rel='author']");
                if (node_username_i != null)
                    param.username = node_username_i.InnerText;
            }

            HtmlAgilityPack.HtmlNode node_avatar = htdoc.DocumentNode.SelectSingleNode("//img[@data-reactid='.0.0.1.2.0.1.0.0.0']");
            if (node_avatar != null)
            {
                HtmlAgilityPack.HtmlAttributeCollection attributes = node_avatar.Attributes;
                if (attributes != null)
                    foreach (HtmlAgilityPack.HtmlAttribute src in attributes.AttributesWithName("src"))
                    {
                        param.avatar_url = src.Value;
                        break;
                    }
            }

            HtmlAgilityPack.HtmlNode node_fio = htdoc.DocumentNode.SelectSingleNode("//strong[@data-reactid='.0.0.1.2.0.2.0']");
            if (node_fio != null)
            {
                param.fio = node_fio.InnerText;
            }

            HtmlAgilityPack.HtmlNode node_description = htdoc.DocumentNode.SelectSingleNode("//span[@data-reactid='.0.0.1.2.0.2.2']");
            if (node_description != null)
            {
                param.description = node_description.InnerText;
            }

            HtmlAgilityPack.HtmlNode node_description2 = htdoc.DocumentNode.SelectSingleNode("//span[@data-reactid='.0.0.1.2.0.2.4']");
            if (node_description2 != null)
            {
                param.description += " " + node_description2.InnerText;
            }
        }

        public static void Parsing(string html, FilterParam filter_param, string path, CefSharp.OffScreen.ChromiumWebBrowser off_screen_browser, CefSharp.WinForms.ChromiumWebBrowser win_form_browser, ref string log, ref CefSharp.WinForms.ChromiumWebBrowser browser2)
        {
            log += "start parsing ";
            GetLikeCommentsLevel(html, ref filter_param);
            int start_node_number = 0;
            int num = 0;
            System.Collections.Generic.List<PhotoGridMediaItem> items = new System.Collections.Generic.List<PhotoGridMediaItem>();
            CalcNumFoto(ref start_node_number, ref num, html, filter_param, items, ref  browser2);

            while (num < filter_param.need_fotos)
            {
                Console.WriteLine();
                Console.WriteLine("Parsing()  while (num < 100) ");

                System.Threading.Tasks.Task<JavascriptResponse> task_js = null;
                
                if (off_screen_browser != null)
                {
                    task_js = off_screen_browser.EvaluateScriptAsync(script);
                }
                else if (win_form_browser != null)
                {
                    task_js = win_form_browser.EvaluateScriptAsync(script);
                }

                if (task_js != null)
                {
                    task_js.Wait();

                    System.Threading.Thread.Sleep(nMillisecondsSleep);

                    JavascriptResponse js_resp = task_js.Result;

                    Console.WriteLine();
                    Console.WriteLine("JavascriptResponse result = {0} ", js_resp.Success);
                }

                System.Threading.Tasks.Task<string> task_html = null;
                if (off_screen_browser != null)
                {
                    task_html = off_screen_browser.GetSourceAsync();
                }
                else if (win_form_browser != null)
                {
                    task_html = win_form_browser.GetSourceAsync();
                }

                if (task_html != null)
                {
                    task_html.Wait();
                    html = task_html.Result;
                }

                int old_num = num;

                CalcNumFoto(ref start_node_number, ref num, html, filter_param, items, ref  browser2);

                if (old_num == num)
                    break;
            }

            log += "num foto calculed ";

            _Parsing(items, path, ref log);
        }

        private static bool GetTime(HtmlAgilityPack.HtmlNode node, ref DateTime time)
        {
            if (node == null)
                return false;

            string html = node.InnerHtml;

            HtmlAgilityPack.HtmlDocument htdoc = new HtmlAgilityPack.HtmlDocument();
            htdoc.LoadHtml(html);

            HtmlAgilityPack.HtmlNode time_node = htdoc.DocumentNode.SelectSingleNode("//time[@class='pgmiDateHeader']");
            if (time_node == null)
                return false;

            string stime = time_node.InnerText;
            Console.WriteLine("stime = \"{0}\"", stime);
            time = DateTime.Parse(stime);
            Console.WriteLine(time);

            return true;
        }

        private static bool GetLikesCommentsNumber(HtmlAgilityPack.HtmlNode node, ref int nLikes, ref int nComments)
        {
            HtmlAgilityPack.HtmlAttributeCollection attributeCollection = node.Attributes;
            if (attributeCollection == null)
                return false;

            nLikes = 0;
            nComments = 0;

            foreach (HtmlAgilityPack.HtmlAttribute attribute in attributeCollection.AttributesWithName("aria-label"))
            {
                //197954 likes, 14453 comments
                string s = attribute.Value;

                int lenLikes = s.IndexOf(" like");
                if (lenLikes < 0)
                    continue;
                string slikes = s.Substring(0, lenLikes);


                int icomments1 = s.IndexOf(", ") + 2;
                if (icomments1 < 0)
                    continue;
                int icomments2 = s.IndexOf(" comment");
                int lenComments = icomments2 - icomments1;
                if (lenComments < 0)
                    continue;
                string scomments = s.Substring(icomments1, lenComments);

                Console.WriteLine("aria-label = \"{0}\" slikes={1} scomments={2}", s, slikes, scomments);

                nLikes = Int32.Parse(slikes);
                nComments = Int32.Parse(scomments);

            }
            Console.WriteLine("nLikes = {0} nComments={1}", nLikes, nComments);
            
            return true;
        }

        private static bool GetImagePageLink(HtmlAgilityPack.HtmlNode node, ref string image_page_link)
        {
            if (node == null)
                return false;

            string html = node.InnerHtml;

            HtmlAgilityPack.HtmlDocument htdoc = new HtmlAgilityPack.HtmlDocument();
            htdoc.LoadHtml(html);

            HtmlAgilityPack.HtmlNode image_link_node = htdoc.DocumentNode.SelectSingleNode("//a[@class='pgmiImageLink']");
            if (image_link_node == null)
                return false;

            HtmlAgilityPack.HtmlAttributeCollection attributeCollection = image_link_node.Attributes;
            if (attributeCollection == null)
                return false;

            foreach (HtmlAgilityPack.HtmlAttribute attribute in attributeCollection.AttributesWithName("href"))
            {
                image_page_link = attribute.Value;

                Console.WriteLine();
                Console.WriteLine("image_page_link = \"{0}\"", image_page_link);
            }
            
            return true;
        }
        
        //по 10 фоткам (которые самые последнии) программа выбирает ту фотку, 
        //где больше всего лайков и комментариев и дальше собирает только те, 
        //у которых лайков и коментов не ниже этого значения
        private static void GetLikeCommentsLevel(string html, ref FilterParam filter_param)
        {
            Console.WriteLine();
            Console.WriteLine("GetLikeCommentsLevel() ");

            HtmlAgilityPack.HtmlDocument htdoc = new HtmlAgilityPack.HtmlDocument();
            htdoc.LoadHtml(html);

            HtmlAgilityPack.HtmlNodeCollection nodes = htdoc.DocumentNode.SelectNodes("//div[@class='PhotoGridMediaItem']");
            if (nodes == null)
            {
                Console.WriteLine();
                Console.WriteLine("(nodes == null)");
                Console.WriteLine("GetLikeCommentsLevel() return 0;");
                return;
            }

            filter_param.nLevelMax10Likes = 0;
            filter_param.nLevelMax10Comments = 0;

            int i = 0;
            foreach (HtmlAgilityPack.HtmlNode node in nodes)
            {
                if (node == null)
                    continue;

                DateTime time = DateTime.MinValue;
                if (!GetTime(node, ref time))
                    continue;
                
                int nLikes = 0, nComments = 0;
                if (!GetLikesCommentsNumber(node, ref nLikes, ref nComments))
                    continue;

                if (filter_param.nLevelMax10Likes < nLikes)
                    filter_param.nLevelMax10Likes = nLikes;

                if (filter_param.nLevelMax10Comments < nComments)
                    filter_param.nLevelMax10Comments = nComments;

                if (++i >= 10)
                    break;
            }
        }

        private static void CalcNumFoto(ref int start_node_number, ref int num, string html, FilterParam filter_param, System.Collections.Generic.List<PhotoGridMediaItem>  items, ref CefSharp.WinForms.ChromiumWebBrowser browser2)
        {
            Console.WriteLine();
            Console.WriteLine("CalcNumFoto() ");

            HtmlAgilityPack.HtmlDocument htdoc = new HtmlAgilityPack.HtmlDocument();
            htdoc.LoadHtml(html);

            HtmlAgilityPack.HtmlNodeCollection nodes = htdoc.DocumentNode.SelectNodes("//div[@class='PhotoGridMediaItem']");
            if (nodes == null)
            {
                Console.WriteLine();
                Console.WriteLine("(nodes == null)");
                Console.WriteLine("GetLikeCommentsLevel() return 0;");
                return;
            }

            int node_number = 0;
            foreach (HtmlAgilityPack.HtmlNode node in nodes)
            {
                if (node_number++ < start_node_number)
                    continue;

                if (node == null)
                    continue;

                DateTime time = DateTime.MinValue;
                if (!GetTime(node, ref time))
                    continue;

                bool bTime = true;
                if (filter_param.use_time_interval_filter)
                {
                    bool bTimeMin = time >= filter_param.time_min;
                    bool bTimeMax = time <= filter_param.time_max;
                    bTime = bTimeMin && bTimeMax;

                    if (!bTimeMin)
                        break;
                }

                int nLikes = 0, nComments = 0;
                if (!GetLikesCommentsNumber(node, ref nLikes, ref nComments))
                    continue;

                string image_page_link = "";
                if (!GetImagePageLink(node, ref image_page_link))
                    continue;



                string image_page_html = "";
                if (!ReadImagePage(image_page_link, ref image_page_html, ref browser2))
                    continue;

                string image_url = "";
                if (!CefSharp.MyParserTask.MyBrowserTask2.GetImageUrl(image_page_html, ref image_url))
                    continue;

                string image_description = "";
                if (CefSharp.MyParserTask.MyBrowserTask2.GetImageDescription(image_page_html, ref image_description))
                {
                }

                bool bLikesComments = true;
                if (filter_param.use_likes_comments_nuber_filter)
                    bLikesComments =  nLikes >= filter_param.nLevelMax10Likes || nComments >= filter_param.nLevelMax10Comments;



                bool bStopWords = true;
                if (filter_param.use_stop_words_filter)
                {
                    foreach (string stop_word in filter_param.stop_words)
                    {
                        if (image_description.Contains(stop_word))
                            bStopWords = false;
                    }
                }

                bool bLoad = bLikesComments && bTime && bStopWords;

                PhotoGridMediaItem item = new PhotoGridMediaItem();
                item.time = time;
                item.nLikes = nLikes;
                item.nComments = nComments;
                item.image_page_link = image_page_link;
                item.image_url = image_url;
                item.image_description = image_description;
                item.bLoad = bLoad;
                items.Add(item);

                if (bLoad)
                {
                    ++num;
                }

            }

            start_node_number = nodes.Count;

            Console.WriteLine();
            Console.WriteLine("CalcNumFoto() num = {0}", num);
        }

        private static void _Parsing(System.Collections.Generic.List<PhotoGridMediaItem> items, string path, ref string log)
        {
            Console.WriteLine();
            Console.WriteLine("_Parsing() open file ");
            string fn = path + "_descriptions.csv";
            log += "_parsing " + fn + " "; 


            System.IO.StreamWriter descriptions = new System.IO.StreamWriter(fn, false, CefSharp.MyParserTask.FirstParser.m_encoding);

            foreach (PhotoGridMediaItem item in items) 
            {
                Console.WriteLine();
                Console.WriteLine("image_url {0}", item.image_url);

                if (item.bLoad)
                {
                    log += "item load " + item.image_url + " ";

                    string dowloadPath = CefSharp.MyParserTask.MyBrowserTask2.SavingImage(item.image_url, path);

                    log += "image saved ";
                    string line = "\"" + item.image_description + "\", " + dowloadPath;
                    descriptions.WriteLine(line, CefSharp.MyParserTask.FirstParser.m_encoding);

                    log += "description line wrote ";
                }
            }

            descriptions.Close();

            Console.WriteLine();
            Console.WriteLine("_Parsing() end");

            log += "_parsing end";
        }

        public static string user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1";
        public static string accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        public static string accept_charset = "windows-1251,utf-8;q=0.7,*;q=0.7";
        public static string accept_language = "ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3";



        private static bool ReadImagePage(string url, ref string html, ref CefSharp.WinForms.ChromiumWebBrowser browser2)
        {
            MyBrowserTask2 browser_task = new MyBrowserTask2(url, ref  browser2);
            html = browser_task.m_html;
            return true;
        }
    }
}
