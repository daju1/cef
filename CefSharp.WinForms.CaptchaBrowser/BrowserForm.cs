﻿// Copyright © 2010-2014 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
using HtmlAgilityPack;

using System;
using System.Windows.Forms;
using CefSharp.Example;
using System.Threading.Tasks;

namespace CefSharp.WinForms.CaptchaBrowser
{
    public partial class BrowserForm : Form
    {
        private const string DefaultUrlForAddedTabs = "https://www.google.com";
        private const string CaptchaUrlForAddedTabs = "http://saiti.protofactory.net/bitcoin/index.php";
        private const string PinkUrlForAddedTabs = "http://pinktussy.co/";
        private const string bitcoin_id = "1G4tXa2SqzxtRHjh91jaRVzkLhthL59nBi";


        public BrowserForm()
        {
            InitializeComponent();

            var bitness = Environment.Is64BitProcess ? "x64" : "x86";
            Text = "CefSharp.WinForms.CaptchaBrowser - " + bitness;
            WindowState = FormWindowState.Maximized;

            AddTab(CefExample.DefaultUrl);

            //Only perform layout when control has completly finished resizing
            ResizeBegin += (s, e) => SuspendLayout();
            ResizeEnd += (s, e) => ResumeLayout(true);
        }

        private void AddTab(string url, int? insertIndex = null)
        {
            browserTabControl.SuspendLayout();

            var browser = new BrowserTabUserControl(url)
            {
                Dock = DockStyle.Fill,
            };

            var tabPage = new TabPage(url)
            {
                Dock = DockStyle.Fill
            };

            //This call isn't required for the sample to work. 
            //It's sole purpose is to demonstrate that #553 has been resolved.
            browser.CreateControl();

            tabPage.Controls.Add(browser);

            if (insertIndex == null)
            {
                browserTabControl.TabPages.Add(tabPage);
            }
            else
            {
                browserTabControl.TabPages.Insert(insertIndex.Value, tabPage);
            }

            //Make newly created tab active
            browserTabControl.SelectedTab = tabPage;

            browserTabControl.ResumeLayout(true);
        }

        private void ExitMenuItemClick(object sender, EventArgs e)
        {
            ExitApplication();
        }

        private void ExitApplication()
        {
            Cef.Shutdown();
            Close();
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void FindMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.ShowFind();
            }
        }

        private void CopySourceToClipBoardAsyncClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.CopySourceToClipBoardAsync();
            }
        }

        private BrowserTabUserControl GetCurrentTabControl()
        {
            if (browserTabControl.SelectedIndex == -1)
            {
                return null;
            }

            var tabPage = browserTabControl.Controls[browserTabControl.SelectedIndex];
            var control = (BrowserTabUserControl)tabPage.Controls[0];

            return control;
        }

        private void NewTabToolStripMenuItemClick(object sender, EventArgs e)
        {
            AddTab(DefaultUrlForAddedTabs);
        }

        private void CloseTabToolStripMenuItemClick(object sender, EventArgs e)
        {
            if(browserTabControl.Controls.Count == 0)
            {
                return;
            }

            var currentIndex = browserTabControl.SelectedIndex;

            var tabPage = browserTabControl.Controls[currentIndex];

            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Dispose();
            }

            browserTabControl.Controls.Remove(tabPage);

            browserTabControl.SelectedIndex = currentIndex - 1;

            if (browserTabControl.Controls.Count == 0)
            {
                ExitApplication();
            }
        }

        private void UndoMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Undo();
            }
        }

        private void RedoMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Redo();
            }
        }

        private void CutMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Cut();
            }
        }

        private void CopyMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Copy();
            }
        }

        private void PasteMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Paste();
            }
        }

        private void DeleteMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Delete();
            }
        }

        private void SelectAllMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.SelectAll();
            }
        }

        private void PrintToolStripMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.Print();
            }
        }

        private void ShowDevToolsMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.ShowDevTools();
            }
        }

        private void CloseDevToolsMenuItemClick(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                control.Browser.CloseDevTools();
            }
        }

        private void newTabToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AddTab(CaptchaUrlForAddedTabs);
        }

        private void newPinkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddTab(PinkUrlForAddedTabs);

        }

        private void analyseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var control = GetCurrentTabControl();
            if (control != null)
            {
                var task = control.Browser.GetSourceAsync();
                /*task.ContinueWith(t =>
                {
                    if (!t.IsFaulted)
                    {
                        Clipboard.SetText(t.Result);
                        string html = t.Result;
                    }
                },
                TaskScheduler.FromCurrentSynchronizationContext());*/

                task.Wait();

                string html = task.Result;
                HtmlAgilityPack.HtmlDocument htdoc = new HtmlAgilityPack.HtmlDocument();
                htdoc.LoadHtml(html);

                //<input class="form-control input-lg" type="text" name="username" id="username" value="" placeholder="Bitcoin address">
                HtmlAgilityPack.HtmlNode node_username = htdoc.DocumentNode.SelectSingleNode("//input[@name='username']");
                if (node_username != null)
                {

                    /*
                     
       window._comscore = window._comscore || [];

       _comscore.push({ c1: "8", c2: "14651931", c3: "1000000000000000001" });

       (function() {
              var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
              s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
              el.parentNode.insertBefore(s, el);
       })();

                     */
                    string script = "var el = document.getElementsByTagName(\"username\")[0]; el.value = \"" + bitcoin_id + "\"";
                    var task_js = control.Browser.EvaluateScriptAsync(script);
                    task_js.Wait();

                    JavascriptResponse responce = task_js.Result;

                    //this.label10.Text = node.NextSibling.InnerText;
                    //resume_id = node.GetAttributeValue("value", "");
                }

                //<div id="adcopy-puzzle-image-image"><iframe src="//api.solvemedia.com/papi/media?c=2@J3uV5vsHCvMiX0HzB6E8QEEED1MMtzMJ@UKah1tYnVAoENU8bgrS38n9APTC4lJjo40GW2-TJbZq.z1rwj2ZeUWXIst2XPdoFgtCgX9jKBG-szoInnWu4a5BmV.2QXOwI42XMxrKZyW72o2WW4NRGg6ob66IyasIKGMldhbThb8qo9cY4r.y.KjWowNNTgQSZ86Dv9T4azqmiMpcJnPFw.PXnE2fTdy8Oy2lS84UFjUuLjVFtafoyS96K27vUXMTbxAEENhdEYiHfSgDdyJNoCYXbt3gANPWnsEuCJKi6R1tVPPjZ4FhFbJPG-tXsV7ZiJkhFXIK0uoA;w=300;h=150;fg=000000;bg=f8f8f8" height="150" width="300" id="adcopy-unique-1418981010422" frameborder="0" scrolling="no"></iframe></div>
                 
                HtmlAgilityPack.HtmlNode node_adcopy_puzzle_image_image = htdoc.DocumentNode.SelectSingleNode("//div[@id='adcopy-puzzle-image-image']");
                if (node_adcopy_puzzle_image_image != null)
                {
                    string code = node_adcopy_puzzle_image_image.InnerHtml;
                    //this.label10.Text = node.NextSibling.InnerText;
                    //resume_id = node.GetAttributeValue("value", "");
                }

            }
        }
    }
}
